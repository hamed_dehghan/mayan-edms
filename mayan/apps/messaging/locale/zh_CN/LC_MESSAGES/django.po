# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# malijie <tonymlj2008@126.com>, 2024
# Ford Guo <agile.guo@gmail.com>, 2024
# Roberto Rosario, 2024
# bo feng <87289884@qq.com>, 2024
# Leon Chu, 2024
# 
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-07 07:29+0000\n"
"PO-Revision-Date: 2024-05-07 07:30+0000\n"
"Last-Translator: Leon Chu, 2024\n"
"Language-Team: Chinese (China) (https://app.transifex.com/rosarior/teams/13584/zh_CN/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: zh_CN\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: apps.py:38 events.py:6 permissions.py:6
msgid "Messaging"
msgstr "消息"

#: apps.py:60
msgid "None"
msgstr "无"

#: apps.py:66
msgid "Sender"
msgstr "发送方"

#: events.py:10
msgid "Message created"
msgstr "消息已创建"

#: events.py:13
msgid "Message edited"
msgstr "消息已编辑"

#: forms.py:26 models.py:40 workflow_actions.py:79
msgid "Body"
msgstr "正文"

#: links.py:51 views.py:38
msgid "Create message"
msgstr "创建消息"

#: links.py:54 links.py:60
msgid "Delete"
msgstr "删除"

#: links.py:70 links.py:81
msgid "Mark as read"
msgstr "标记为读"

#: links.py:76 links.py:85
msgid "Mark as unread"
msgstr ""

#: links.py:89
msgid "Mark all as read"
msgstr "全部标记为读"

#: model_mixins.py:16
msgid "Label"
msgstr "标签"

#: models.py:21
msgid "Sender content type"
msgstr ""

#: models.py:24
msgid "Sender object ID"
msgstr ""

#: models.py:31
msgid "User"
msgstr "用户"

#: models.py:35
msgid "Short description of this message."
msgstr "此消息的简短描述。"

#: models.py:36 workflow_actions.py:64
msgid "Subject"
msgstr "主题"

#: models.py:39
msgid "The actual content of the message."
msgstr "消息的实际内容。"

#: models.py:44
msgid "This field determines if the message has been read or not."
msgstr "此字段确定消息是否已读。"

#: models.py:46
msgid "Read"
msgstr "读取"

#: models.py:50
msgid "Date and time of the message creation."
msgstr "消息创建的日期和时间。"

#: models.py:51
msgid "Creation date and time"
msgstr "创建日期和时间"

#: models.py:56
msgid "Message"
msgstr "消息"

#: models.py:57 views.py:128
msgid "Messages"
msgstr "消息"

#: permissions.py:10
msgid "Create messages"
msgstr "创建消息"

#: permissions.py:13
msgid "Delete messages"
msgstr "删除消息"

#: permissions.py:16
msgid "Edit messages"
msgstr "编辑消息"

#: permissions.py:19
msgid "View messages"
msgstr "查看消息"

#: serializers.py:15
msgid "Sender app label"
msgstr ""

#: serializers.py:18
msgid "Sender model name"
msgstr ""

#: serializers.py:21
msgid "Sender URL"
msgstr ""

#: serializers.py:25
msgid "Primary key of the recipient user of this message."
msgstr ""

#: serializers.py:26
msgid "User ID"
msgstr "用户ID"

#: serializers.py:33
msgid "URL"
msgstr "网址"

#: views.py:49
#, python-format
msgid "Error deleting message \"%(instance)s\"; %(exception)s"
msgstr "刪除訊息\"%(instance)s\"錯誤;%(exception)s"

#: views.py:53
#, python-format
msgid "%(count)d messages deleted successfully."
msgstr "%(count)d標示訊息刪除成功"

#: views.py:54
#, python-format
msgid "Message \"%(object)s\" deleted successfully."
msgstr "訊息 \"%(object)s\"刪除成功"

#: views.py:55
#, python-format
msgid "%(count)d message deleted successfully."
msgstr "%(count)d訊息刪除成功"

#: views.py:56
#, python-format
msgid "Delete the %(count)d selected messages."
msgstr "刪除%(count)d已選擇之訊息"

#: views.py:57
#, python-format
msgid "Delete message: %(object)s."
msgstr "刪除訊息 : %(object)s."

#: views.py:58
#, python-format
msgid "Delete the %(count)d selected message."
msgstr "刪除%(count)d已選擇訊息"

#: views.py:100
#, python-format
msgid "Details of message: %s"
msgstr ""

#: views.py:124
msgid "Here you will find text messages from other users or from the system."
msgstr "在这里，您将找到来自其他用户或系统的消息。"

#: views.py:127
msgid "There are no messages"
msgstr "没有任何消息"

#: views.py:137
#, python-format
msgid "Error marking message \"%(instance)s\" as read; %(exception)s"
msgstr "標示訊息\"%(instance)s\"為已讀失敗%(exception)s"

#: views.py:143
#, python-format
msgid "%(count)d messages marked as read successfully."
msgstr "%(count)d訊息標示為已讀成功"

#: views.py:146
#, python-format
msgid "Message \"%(object)s\" marked as read successfully."
msgstr ""

#: views.py:149
#, python-format
msgid "%(count)d message marked as read successfully."
msgstr "%(count)d標示訊息為已讀成功"

#: views.py:151
#, python-format
msgid "Mark the %(count)d selected messages as read."
msgstr "標示%(count)d所選擇之訊息為已讀"

#: views.py:152
#, python-format
msgid "Mark the message \"%(object)s\" as read."
msgstr "標示訊息\"%(object)s\"為已讀"

#: views.py:153
#, python-format
msgid "Mark the %(count)d selected message as read."
msgstr "標示%(count)d已選擇訊息為已讀"

#: views.py:181
msgid "Mark all message as read?"
msgstr "将所有消息标记为读？"

#: views.py:197
msgid "All messages marked as read."
msgstr "所有消息都已标记为读。"

#: views.py:204
#, python-format
msgid "Error marking message \"%(instance)s\" as unread; %(exception)s"
msgstr ""

#: views.py:210
#, python-format
msgid "%(count)d messages marked as unread successfully."
msgstr ""

#: views.py:213
#, python-format
msgid "Message \"%(object)s\" marked as unread successfully."
msgstr ""

#: views.py:216
#, python-format
msgid "%(count)d message marked as unread successfully."
msgstr ""

#: views.py:218
#, python-format
msgid "Mark the %(count)d selected messages as unread."
msgstr ""

#: views.py:219
#, python-format
msgid "Mark the message \"%(object)s\" as unread."
msgstr ""

#: views.py:220
#, python-format
msgid "Mark the %(count)d selected message as unread."
msgstr ""

#: workflow_actions.py:20
msgid "Group name list"
msgstr ""

#: workflow_actions.py:24
msgid ""
"Comma separated list of user group names that will receive the message. Can "
"be a static value or a template."
msgstr ""

#: workflow_actions.py:34
msgid "Role name list"
msgstr ""

#: workflow_actions.py:38
msgid ""
"Comma separated list of role labels that will receive the message. Can be a "
"static value or a template."
msgstr ""

#: workflow_actions.py:48
msgid "Username list"
msgstr ""

#: workflow_actions.py:53
msgid ""
"Comma separated list of usernames that will receive the message. Can be a "
"static value or a template."
msgstr ""

#: workflow_actions.py:69
msgid ""
"Subject of the message to be sent. Can be a static value or a template."
msgstr ""

#: workflow_actions.py:84
msgid "The actual text to send. Can be a static value or a template."
msgstr ""

#: workflow_actions.py:94
msgid "Send user message"
msgstr ""

#: workflow_actions.py:102
msgid "Recipients"
msgstr ""

#: workflow_actions.py:108
msgid "Content"
msgstr "内容"
