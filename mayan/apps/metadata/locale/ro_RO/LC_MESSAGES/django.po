# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Translators:
# Roberto Rosario, 2024
# Stefaniu Criste <gupi@hangar.ro>, 2024
# Badea Gabriel <gcbadea@gmail.com>, 2024
# Harald Ersch, 2024
# 
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-06-04 06:21+0000\n"
"PO-Revision-Date: 2024-05-07 07:30+0000\n"
"Last-Translator: Harald Ersch, 2024\n"
"Language-Team: Romanian (Romania) (https://app.transifex.com/rosarior/teams/13584/ro_RO/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ro_RO\n"
"Plural-Forms: nplurals=3; plural=(n==1?0:(((n%100>19)||((n%100==0)&&(n!=0)))?2:1));\n"

#: apps.py:71 apps.py:209 apps.py:215 apps.py:219 apps.py:224 apps.py:228
#: events.py:6 links.py:53 permissions.py:6 queues.py:7
#: workflow_actions.py:162
msgid "Metadata"
msgstr "Metadate"

#: apps.py:141
msgid "Return the value of a specific document metadata."
msgstr "Întoarceți valoarea unei metadate de documente specifice."

#: apps.py:142
msgid "Metadata value of"
msgstr "Valoarea metadatelor pentru"

#: apps.py:147
msgid "Metadata type name"
msgstr "Numele tipului de metadate"

#: apps.py:151 search.py:14
msgid "Metadata value"
msgstr "Valoare metadate "

#: events.py:10
msgid "Document metadata added"
msgstr "Au fost adăugate metadate la document"

#: events.py:13
msgid "Document metadata edited"
msgstr "Au fost editate metadatele pentru document"

#: events.py:16
msgid "Document metadata removed"
msgstr "Metadatele documentelelor au fost eliminate"

#: events.py:19
msgid "Metadata type created"
msgstr "Tipul de metadate a fost creat"

#: events.py:22
msgid "Metadata type edited"
msgstr "Tipul de metadate a fost editat"

#: events.py:25
msgid "Metadata type relationship updated"
msgstr "Relația tipului de metadate a fost actualizată"

#: forms.py:15
msgid "ID"
msgstr "ID-ul"

#: forms.py:18 models/metadata_type_models.py:35
msgid "Name"
msgstr "Nume"

#: forms.py:26 models/metadata_instance_models.py:43 workflow_actions.py:112
msgid "Value"
msgstr "Valoare"

#: forms.py:31
msgid "Update"
msgstr "Actualizați"

#: forms.py:51 forms.py:232 models/document_type_metadata_type_models.py:31
msgid "Required"
msgstr "Necesară"

#: forms.py:83
#, python-format
msgid "Lookup value error: %s"
msgstr "Valoarea erorii de căutare: %s"

#: forms.py:96
#, python-format
msgid "Default value error: %s"
msgstr "Valoare eroare standard: %s"

#: forms.py:117 models/metadata_type_models.py:120
#, python-format
msgid "\"%s\" is required for this document type."
msgstr "\"%s\" este necesar pentru acest tip de document."

#: forms.py:136
msgid "Metadata types to be added to the selected documents."
msgstr ""
"Tipurile de metadate care urmează să fie adăugate la documentele selectate."

#: forms.py:137 models/document_type_metadata_type_models.py:28
#: models/metadata_type_models.py:83 search.py:11 serializers.py:52
#: serializers.py:114 workflow_actions.py:148
msgid "Metadata type"
msgstr "Tip de metadate"

#: forms.py:162
msgid "Remove"
msgstr "Eliminați"

#: forms.py:181
msgid "Basic"
msgstr "De bază"

#: forms.py:185
msgid "Values"
msgstr "Valori"

#: forms.py:189
msgid "Validation"
msgstr "Validare"

#: forms.py:193
msgid "Parsing"
msgstr "Analizare"

#: forms.py:230
msgid "None"
msgstr "Nici unul"

#: forms.py:231
msgid "Optional"
msgstr "Facultativ"

#: links.py:26 links.py:35 workflow_actions.py:31
msgid "Add metadata"
msgstr "Adăugați metadate"

#: links.py:32 links.py:39 workflow_actions.py:125
msgid "Edit metadata"
msgstr "Editați metadatele"

#: links.py:43 links.py:49 workflow_actions.py:204
msgid "Remove metadata"
msgstr "Eliminați metadatele"

#: links.py:63 links.py:95 links.py:103 models/metadata_type_models.py:84
#: permissions.py:24 views/metadata_type_views.py:112 workflow_actions.py:55
#: workflow_actions.py:69
msgid "Metadata types"
msgstr "Tipuri de metadate"

#: links.py:72
msgid "Document types"
msgstr "Tipuri de documente"

#: links.py:76
msgid "Create new"
msgstr "Crează nou"

#: links.py:82 links.py:87
msgid "Delete"
msgstr "Șterge"

#: links.py:92
msgid "Edit"
msgstr "Editați"

#: metadata_parsers.py:11
msgid "Date and time parser"
msgstr "Analizor de dată și oră"

#: metadata_parsers.py:18
msgid "Date parser"
msgstr "Analizor de dată"

#: metadata_parsers.py:26
msgid "Regular expression parser"
msgstr "Analizor de expresii regulate"

#: metadata_parsers.py:36
msgid "Time parser"
msgstr "Analizor de timp"

#: metadata_validators.py:12
msgid "Date and time validator"
msgstr "Validator de dată și oră"

#: metadata_validators.py:19
msgid "Date validator"
msgstr "Validator de dată"

#: metadata_validators.py:27
msgid "Regular expression validator"
msgstr "Validator de expresii regulate"

#: metadata_validators.py:35
msgid "The input string does not match the pattern."
msgstr "Șirul de intrare nu se potrivește cu modelul."

#: metadata_validators.py:40
msgid "Time validator"
msgstr "Validator de timp"

#: methods.py:20
msgid "Return the metadata of the document."
msgstr "Returnează metadatele documentului."

#: mixins.py:23
msgid "Selected documents must be of the same type."
msgstr "Documentele selectate trebuie să fie de același tip."

#: models/document_type_metadata_type_models.py:24 serializers.py:49
msgid "Document type"
msgstr "Tipul documentului"

#: models/document_type_metadata_type_models.py:39
msgid "Document type metadata type options"
msgstr "Opțiune tip metadate tip document"

#: models/document_type_metadata_type_models.py:41
msgid "Document type metadata types options"
msgstr "Opțiuni tipuri de  metadate tip document"

#: models/metadata_instance_models.py:32 serializers.py:111
msgid "Document"
msgstr "Document"

#: models/metadata_instance_models.py:36
msgid "Type"
msgstr "Tip"

#: models/metadata_instance_models.py:41
msgid "The actual value stored in the metadata type field for the document."
msgstr "Valoarea reală stocată în câmpul tip de metadate pentru document."

#: models/metadata_instance_models.py:49 models/metadata_instance_models.py:50
msgid "Document metadata"
msgstr "Metadatele documentului"

#: models/metadata_instance_models.py:86
msgid "Metadata type is required for this document type."
msgstr "Tipul de metadate este necesar pentru acest tip de document."

#: models/metadata_instance_models.py:120
msgid "Metadata type is not valid for this document type."
msgstr "Tipul de metadate nu este valabil pentru acest tip de document."

#: models/metadata_type_models.py:32
msgid ""
"Name used by other apps to reference this metadata type. Do not use python "
"reserved words, or spaces."
msgstr ""
"Numele utilizat de alte aplicații pentru a face referire la acest tip de "
"metadate. Nu utilizați cuvinte sau spații rezervate Python."

#: models/metadata_type_models.py:38
msgid "Short description of this metadata type."
msgstr "Scurtă descriere a acestui tip de metadate."

#: models/metadata_type_models.py:39
msgid "Label"
msgstr "Conținut etichetă"

#: models/metadata_type_models.py:43
msgid "Enter a template to render."
msgstr "Introduceți un șablon de redat."

#: models/metadata_type_models.py:44
msgid "Default"
msgstr "Implicit"

#: models/metadata_type_models.py:48
msgid "Enter a template to render. Must result in a comma delimited string."
msgstr ""
"Introduceți un șablon de redat. Trebuie să aibă ca rezultat un șir delimitat"
" de virgule."

#: models/metadata_type_models.py:50
msgid "Lookup"
msgstr "Căutare"

#: models/metadata_type_models.py:54
msgid ""
"The validator will reject data entry if the value entered does not conform "
"to the expected format."
msgstr ""
"Validatorul va respinge introducerea datelor dacă valoarea introdusă nu este"
" conformă cu formatul așteptat."

#: models/metadata_type_models.py:56
msgid "Validator"
msgstr "Validator"

#: models/metadata_type_models.py:60
msgid "Enter the arguments for the validator in YAML format."
msgstr "Introduceți argumentele validatorului în format YAML."

#: models/metadata_type_models.py:62
msgid "Validator arguments"
msgstr "Argumente pentru validator"

#: models/metadata_type_models.py:67
msgid ""
"The parser will reformat the value entered to conform to the expected "
"format."
msgstr ""
"Parserul va reformata valoarea introdusă pentru a se conforma formatului "
"așteptat."

#: models/metadata_type_models.py:69
msgid "Parser"
msgstr "Parser"

#: models/metadata_type_models.py:73
msgid "Enter the arguments for the parser in YAML format."
msgstr "Introduceți argumentele analizorului  în format YAML."

#: models/metadata_type_models.py:76
msgid "Parser arguments"
msgstr "Argumente ale analizorului"

#: models/metadata_type_models.py:130
msgid "Value is not one of the provided options."
msgstr "Valoarea nu este una dintre opțiunile furnizate."

#: models/metadata_type_models.py:145
#, python-format
msgid "Metadata type validation error; %(exception)s"
msgstr "Eroare de validare a tipului de metadate; %(exception)s"

#: permissions.py:10
msgid "Add metadata to a document"
msgstr "Adaugă metadate la un document"

#: permissions.py:13
msgid "Edit a document's metadata"
msgstr "Editați metadatele unui document"

#: permissions.py:16
msgid "Remove metadata from a document"
msgstr "Eliminarea metadatelor dintr-un document"

#: permissions.py:20
msgid "View metadata from a document"
msgstr "Vezi metadatele dintr-un document"

#: permissions.py:28
msgid "Edit metadata types"
msgstr "Editați tipuri de metadate"

#: permissions.py:31
msgid "Create new metadata types"
msgstr "Crearea de noi tipuri de metadate"

#: permissions.py:34
msgid "Delete metadata types"
msgstr "Ștergeți tipuri de metadate"

#: permissions.py:37
msgid "View metadata types"
msgstr "Vezi tipuri de metadate"

#: queues.py:11
msgid "Remove metadata type"
msgstr "Eliminați tipul de metadate"

#: queues.py:15
msgid "Add required metadata type"
msgstr "Adăugați tipul de metadate necesar"

#: serializers.py:32 serializers.py:61 serializers.py:117
msgid "URL"
msgstr "URL"

#: serializers.py:56
msgid "Primary key of the metadata type to be added."
msgstr "Cheia primară a tipului de metadate care urmează să fie adăugată."

#: serializers.py:57 serializers.py:107
msgid "Metadata type ID"
msgstr "ID tip de metadate"

#: serializers.py:105
msgid "Primary key of the metadata type to be added to the document."
msgstr ""
"Cheia primară a tipului de metadate care urmează să fie adăugată în "
"document."

#: views/document_views.py:46
#, python-format
msgid "Metadata add request performed on %(count)d document"
msgstr "Solicitarea de adăugare de metadate efectuată pe %(count)ddocument "

#: views/document_views.py:49
#, python-format
msgid "Metadata add request performed on %(count)d documents"
msgstr "Solicitarea de adăugare de metadate efectuată pe %(count)ddocumente"

#: views/document_views.py:58
msgid "Add metadata types to document"
msgid_plural "Add metadata types to documents"
msgstr[0] "Adăugați tipuri de metadate la document"
msgstr[1] "Adăugați tipuri de metadate la documente"
msgstr[2] "Adăugați tipuri de metadate la documente"

#: views/document_views.py:69
#, python-format
msgid "Add metadata types to document: %s"
msgstr "Adăugați tipuri de metadate în documentul: %s"

#: views/document_views.py:147
#, python-format
msgid ""
"Error adding metadata type \"%(metadata_type)s\" to document: %(document)s; "
"%(exception)s"
msgstr ""
"Eroare la adăugarea tipului de metadate \"%(metadata_type)s\" în documentul:"
" %(document)s; %(exception)s"

#: views/document_views.py:166
#, python-format
msgid ""
"Metadata type: %(metadata_type)s successfully added to document "
"%(document)s."
msgstr ""
"Tipul de metadate:%(metadata_type)s a fost adăugat cu succes la documentul "
"%(document)s."

#: views/document_views.py:176
#, python-format
msgid ""
"Metadata type: %(metadata_type)s already present in document %(document)s."
msgstr ""
"Tipul de metadate:%(metadata_type)s e deja prezent în documentul "
"%(document)s."

#: views/document_views.py:193
#, python-format
msgid "Metadata edit request performed on %(count)d document"
msgstr "Cererea de editare de metadate efectuată pe %(count)ddocument"

#: views/document_views.py:196
#, python-format
msgid "Metadata edit request performed on %(count)d documents"
msgstr "Cererea de editare de metadate efectuată pe %(count)ddocumente"

#: views/document_views.py:231
msgid ""
"Add metadata types available for this document's type and assign them "
"corresponding values."
msgstr ""
"Adăugați tipuri de metadate disponibile pentru tipul acestui document și "
"atribuiți-le valorile corespunzătoare."

#: views/document_views.py:234
msgid "There is no metadata to edit"
msgstr "Nu există metadate pentru a fi editate"

#: views/document_views.py:236
msgid "Edit document metadata"
msgid_plural "Edit documents metadata"
msgstr[0] "Editați metadatele documentului"
msgstr[1] "Editați metadatele documentelor"
msgstr[2] "Editați metadatele documentelor"

#: views/document_views.py:247
#, python-format
msgid "Edit metadata for document: %s"
msgstr "Editați metadate pentru document:% s"

#: views/document_views.py:334
#, python-format
msgid "Error editing metadata for document: %(document)s; %(exception)s."
msgstr ""
"Eroare la editarea metadatelor pentru document: %(document)s; %(exception)s."

#: views/document_views.py:347
#, python-format
msgid "Metadata for document %s edited successfully."
msgstr "Metadatele pentru documentul %s au fost editate cu succes."

#: views/document_views.py:372
msgid ""
"Add metadata types this document's type to be able to add them to individual"
" documents. Once added to individual document, you can then edit their "
"values."
msgstr ""
"Adăugați tipuri de metadate pentru acest tip de document pentru a le putea "
"adăuga la documente individuale. După ce ați adăugat la un document "
"individual, puteți să le modificați valorile."

#: views/document_views.py:377
msgid "This document doesn't have any metadata"
msgstr "Acest document nu conține metadate"

#: views/document_views.py:380
#, python-format
msgid "Metadata for document: %s"
msgstr "Metadate pentru documentul: %s"

#: views/document_views.py:396
#, python-format
msgid "Metadata remove request performed on %(count)d document"
msgstr "Solicitarea de eliminare a metadatelor efectuată la %(count)ddocument"

#: views/document_views.py:399
#, python-format
msgid "Metadata remove request performed on %(count)d documents"
msgstr "Cererea de eliminare a metadatelor efectuată pe %(count)ddocumente "

#: views/document_views.py:409
msgid "Remove metadata types from the document"
msgid_plural "Remove metadata types from the documents"
msgstr[0] "Eliminați tipurile de metadate din document"
msgstr[1] "Eliminați tipurile de metadate din documente"
msgstr[2] "Eliminați tipuri de metadate din documente"

#: views/document_views.py:420
#, python-format
msgid "Remove metadata types from the document: %s"
msgstr "Eliminați tipurile de metadate din documentul: %s"

#: views/document_views.py:486
#, python-format
msgid ""
"Successfully remove metadata type \"%(metadata_type)s\" from document: "
"%(document)s."
msgstr ""
"Ttipul de metadate \"%(metadata_type)s\"  a fost șters cu succes din "
"documentul: %(document)s."

#: views/document_views.py:496
#, python-format
msgid ""
"Error removing metadata type \"%(metadata_type)s\" from document: "
"%(document)s; %(exception)s"
msgstr ""
"Eroare la eliminarea tipului de metadate \"%(metadata_type)s\" din "
"documentul: %(document)s; %(exception)s"

#: views/metadata_type_views.py:31
msgid "Create metadata type"
msgstr "Creați un tip de metadate"

#: views/metadata_type_views.py:47
#, python-format
msgid "Error deleting metadata type \"%(instance)s\"; %(exception)s"
msgstr "Eroare la ștergerea tipului de metadate „%(instance)s”; %(exception)s"

#: views/metadata_type_views.py:56
#, python-format
msgid "%(count)d metadata types deleted successfully."
msgstr "%(count)d tipuri de metadate au fost șterse cu succes."

#: views/metadata_type_views.py:59
#, python-format
msgid "Metadata type \"%(object)s\" deleted successfully."
msgstr "Tipul de metadate „%(object)s” a fost șters cu succes."

#: views/metadata_type_views.py:62
#, python-format
msgid "%(count)d metadata type deleted successfully."
msgstr "Tipul de metadate %(count)d a fost șters cu succes."

#: views/metadata_type_views.py:64
#, python-format
msgid "Delete the %(count)d selected metadata types."
msgstr "Ștergeți %(count)d tipuri de metadate selectate."

#: views/metadata_type_views.py:65
#, python-format
msgid "Delete metadata type: %(object)s."
msgstr "Ștergeți tipul de metadate: %(object)s."

#: views/metadata_type_views.py:66
#, python-format
msgid "Delete the %(count)d selected metadata type."
msgstr "Ștergeți %(count)d  tip de metadate selectat. "

#: views/metadata_type_views.py:83
#, python-format
msgid "Edit metadata type: %s"
msgstr "Editați tipul de metadate: %s"

#: views/metadata_type_views.py:104
msgid ""
"Metadata types are user defined properties that can be assigned values. Once"
" created they must be associated to document types, either as optional or "
"required, for each. Setting a metadata type as required for a document type "
"will block the upload of documents of that type until a metadata value is "
"provided."
msgstr ""
"Tipurile de metadate sunt proprietăți definite de utilizator cărora li se "
"pot atribui valori. Odată create, acestea trebuie să fie asociate tipurilor "
"de documente, fie opționale, fie obligatorii, pentru fiecare. Setarea unui "
"tip de metadate așa cum este necesar pentru un tip de document va bloca "
"încărcarea documentelor de acest tip până când este furnizată o valoare de "
"metadate."

#: views/metadata_type_views.py:111
msgid "There are no metadata types"
msgstr "Nu există tipuri de metadate"

#: views/metadata_type_views.py:135
msgid ""
"Create metadata type relationships to be able to associate them to this "
"document type."
msgstr ""
"Creați relații de tip de metadate pentru a le putea asocia cu acest tip de "
"document."

#: views/metadata_type_views.py:139
msgid "There are no metadata type relationships available"
msgstr "Nu există relații de tip metadate disponibile"

#: views/metadata_type_views.py:143
#, python-format
msgid "Metadata type relationships for document type: %s"
msgstr "Relații de tip metadate pentru tipul de document: %s"

#: views/metadata_type_views.py:186
#, python-format
msgid "Document type relationships for metadata type: %s"
msgstr "Relații între tipul de document și tipul de metadate: %s"

#: wizard_steps.py:21
msgid "Enter document metadata"
msgstr "Introduceți metadatele documentului"

#: wizard_steps.py:203
msgid ""
"One of more metadata types that are required for this document type are not "
"available."
msgstr ""
"Unul dintre mai multe tipuri de metadate care sunt necesare pentru acest tip"
" de document nu sunt disponibile."

#: workflow_actions.py:49
msgid "Metadata types to add to the document."
msgstr "Tipuri de metadate de adăugat la document."

#: workflow_actions.py:84
#, python-format
msgid ""
"Unable to add metadata type \"%(metadata_type)s\" from document: "
"%(document)s. Exception: %(exception)s"
msgstr ""
"Imposibil de adăugat metadatele de tip „%(metadata_type)s” din documentul: "
"%(document)s. Excepție: %(exception)s"

#: workflow_actions.py:116
msgid ""
"Value to assign to the metadata. Can be a literal value or template code."
msgstr ""
"Valoare de atribuit metadatelor. Poate fi o valoare literală sau un cod "
"șablon."

#: workflow_actions.py:142
msgid "Metadata types to edit."
msgstr "Tipuri de metadate de editat."

#: workflow_actions.py:181
#, python-format
msgid ""
"Unable to edit metadata type \"%(metadata_type)s\" from document: "
"%(document)s. Document does not have the metadata type to be edited. "
"Exception: %(exception)s"
msgstr ""
"Nu se poate edita metadatele tip „%(metadata_type)s” din documentul: "
"%(document)s. Documentul nu are tipul de metadate care trebuie editat. "
"Excepție: %(exception)s"

#: workflow_actions.py:212
msgid "Metadata types to remove from the document."
msgstr "Tipuri de metadate de eliminat din document."

#: workflow_actions.py:228
#, python-format
msgid ""
"Unable to remove metadata type \"%(metadata_type)s\" from document: "
"%(document)s. Exception: %(exception)s"
msgstr ""
"Imposibil de eliminat metadatele de tip „%(metadata_type)s” din documentul: "
"%(document)s. Excepție: %(exception)s"
